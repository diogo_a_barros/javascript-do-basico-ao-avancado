const data = /[0-9]{2}[/][0-9]{2}[/][0-9]{4}/;

console.log(data.test("05/02/2000"));
console.log(data.test("5/2/2000"));
console.log(data.test("05-02-2000"));
console.log(data.test("05-02-2000"));
console.log(data.test("12/12/1999"));
console.log(data.test("99/99/9999"));
console.log(data.test("31/02/2000"));
