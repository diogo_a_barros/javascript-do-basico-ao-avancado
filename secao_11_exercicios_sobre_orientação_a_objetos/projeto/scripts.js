/**
 * Exercicio 5;
 * Crie uma classe conta bancaria;
 * Com as propriedades de saldo na conta corrente, saldo na conta poupança e juros da poupança;
 * Crie os metodos de deposito e saque, tambem um metodo para transferir dinheiro da poupança para a conta corrente;
 * Alem disso crie uma conta especial que herda da cona normal;
 * Na conta especial os juros são dobrados da conta normal;
 */

class ContaBancaria{
    constructor (saldoCC, saldoCP, juros){
        this.saldoCC = saldoCC;
        this.saldoCP = saldoCP;
        this.juros = juros;
    }

    deposito(valor){
        this.saldoCC += valor;
    }
    
    saque(valor){
        this.saldoCC -= valor;
    }

    transferenciaCCCP(valor){
        this.saldoCC -= valor;
        this.saldoCP += valor;
    }

    transferenciaCPCC(valor){
        this.saldoCC += valor;
        this.saldoCP -= valor;
    }


    jurosDeAniversario(){
        let valorJuros = (this.saldoCP * this.juros) / 100;
        this.saldoCP += valorJuros;
    }
}

class ContaBancariaEspecial extends ContaBancaria {
    constructor (saldoCC, saldoCP, juros){
        super(saldoCC, saldoCP, (juros * 2));
    }
}


let conta = new ContaBancaria(1000, 5000, 0.5);

console.log(conta);

conta.deposito(5000);

console.log(conta);

conta.saque(1500);

console.log(conta);

conta.transferenciaCCCP(1500)

console.log(conta);

conta.transferenciaCPCC(2500)

console.log(conta);

conta.jurosDeAniversario();

console.log(conta);

let conta2 = new ContaBancariaEspecial(10000, 50000, 0.5);

console.log(conta2);

conta2.saque(5000);

console.log(conta2);

conta2.jurosDeAniversario();

console.log(conta2);
