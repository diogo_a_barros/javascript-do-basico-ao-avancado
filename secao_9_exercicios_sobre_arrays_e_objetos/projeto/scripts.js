/**
 * Exercicio 10;
 * Crie um objeto calculadora;
 * Que tem os seguintes metodos: somar, subtrair, multiplicar e dividir;
 * Os metodos só devem aceitar dois parametros;
 * Utilize cada um dos métodos e imprima os valores no console.
*/

const Calculadora = {
    somar: function (a, b) {
        return a + b;
    },

    subtrair: function (a, b) {
        return a - b;
    },

    multiplicar: function(a, b) {
        return a * b;
    },

    dividir: function(a, b) {
        return a / b;
    }
}

console.log(Calculadora.somar(12, 8));
console.log(Calculadora.subtrair(12, 8));
console.log(Calculadora.multiplicar(12, 8));
console.log(Calculadora.dividir(12, 8));
