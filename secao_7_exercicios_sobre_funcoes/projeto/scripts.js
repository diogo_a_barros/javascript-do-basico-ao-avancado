/**
 * Exercicio 10;
 * Escreva uma fun��o que receba um n�mero e o decremente de 1 em 1 com um loop;
 * Alem disso escreva somente os numeros pares no console; 
*/

function decrementoPar(numero){
    for (let index = numero; index >= 0; index--) {
        if (index % 2 == 0){
            console.log(index);
        }    
    }
}

decrementoPar(1000);