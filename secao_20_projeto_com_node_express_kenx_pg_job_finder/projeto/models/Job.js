const { Sequelize } = require('sequelize');
const db = require('../db/connection');

const Job = db.define('jobs', {
    title: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    salary: {
        type: Sequelize.DECIMAL
    },
    company: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    new_job: {
        type: Sequelize.BOOLEAN
    }
})

module.exports = Job;
