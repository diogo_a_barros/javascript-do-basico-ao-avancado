drop table if exists jobs;

create table jobs(
	id bigserial primary key,
	title varchar,
	description varchar,
	salary numeric,
	company varchar,
	email varchar,
	new_job boolean,
	"createdAt" timestamp without time zone,
	"updatedAt" timestamp without time zone
)