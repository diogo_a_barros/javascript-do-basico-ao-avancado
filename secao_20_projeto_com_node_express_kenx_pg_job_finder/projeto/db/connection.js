const { Sequelize } = require('sequelize');

const sequelize = new Sequelize({
    dialect: 'postgres',
    host: '127.0.0.1',
    port: 5435,
    database: 'app',
    username: 'postgres',
    password: ''
})

module.exports = sequelize;