class Mamifero{
    constructor(patas){
        this.patas = patas;
    }
}

let Coiote = new Mamifero(4);

console.log(Coiote);

class Cachorro extends Mamifero{
    constructor (patas, raca){
        super(patas, patas);
        this.raca = raca;
    }

    latir(){
        console.log("Au au");
    }
}

let viraLata = new Cachorro(4, "Vira-Latas");

console.log(viraLata);
viraLata.latir();

console.log(new Cachorro instanceof Mamifero);
console.log(Coiote instanceof Mamifero);