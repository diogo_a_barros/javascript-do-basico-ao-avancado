let express = require('express');
let app = express();

app.get('/', function(req, res){
    res.send('Primeira rota com express');
})

app.listen(3000, function(){
    console.log("Aplicação rodando na porta 3000");
})

app.get('/teste', function(req, res){
    res.send('Testando rota com express');
})