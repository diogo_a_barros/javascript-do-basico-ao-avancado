let pessoa = {
    "nome": "João",
    "idade": 28,
    "profissao": "programador",
    "hobbies": ["Video Game", "Leitura", "Correr"]
}

let pessoaTexto = JSON.stringify(pessoa);
let pessoaJSON = JSON.parse(pessoaTexto);
console.log(pessoa);
console.log(pessoaTexto);
console.log(pessoaJSON);