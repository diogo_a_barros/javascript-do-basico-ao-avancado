/**
 * Exercicio 5
 * Crie uma regex que valide nome de usuarios no sistema;
 * Aceita letras de a-z, números de 0-9, minimo de 3 caracteres e maximo de 16;
 * Depois teste;
 */

const validaNomeUsuario = /^(?=.{3,16}$)[a-z0-9-_]/;

console.log(validaNomeUsuario.test("teste123"));
console.log(validaNomeUsuario.test("teste_123"));
console.log(validaNomeUsuario.test("t1"));
console.log(validaNomeUsuario.test("01234567890"));
console.log(validaNomeUsuario.test("teste012345678901"));



