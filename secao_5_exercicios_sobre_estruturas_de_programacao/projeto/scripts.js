/**
 * Exercicio 15
 ** Verifique se o n�mero � primo;
 ** Um n�mero � primo, � um n�mero natural, maior que 1 e apenas divis�vel por si pr�prio e por 1;
 */

let numero = 100;
let ehPrimo = true;

for (let i = 1; i <= numero; i ++){
    if (i == 1 || i == numero){
        continue;
    }
    if ((numero % i) == 0){
        ehPrimo = false;
        break;
    }
}

if (ehPrimo){
    console.log(`O n�mero ${numero} � primo!`);
} else {
    console.log(`O n�mero ${numero} n�o � primo!`);
}